#include <iostream>
#include <signal.h>
#include <mosquitto.h>
#include <unistd.h>
#include <nlohmann/json.hpp>
#include <wiringPi.h>

using json = nlohmann::json;
using namespace std;

#define mqtt_host "161.97.130.237"
#define mqtt_port 1883


static int run = 1;

void handle_signal(int s)
{
	run = 0;
}

void connect_callback(struct mosquitto *mosq, void *obj, int result)
{
	printf("connect callback, rc=%d\n", result);
}

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	//bool match = 0;
	//printf("got message '%.*s' for topic '%s'\n", message->payloadlen, (char*) message->payload, message->topic);
   // printf("%s", (char*) message->payload );

    json received = json::parse((char*)message->payload);



    if(received["status"]=="O"){
         digitalWrite(23,LOW);
		 sleep(1);
    }
    else {
        digitalWrite(23,HIGH);
		sleep(1);
	}

}

int main(int argc, char *argv[])
{
	uint8_t reconnect = true;
	const char* clientid = "macktosha";
	struct mosquitto *mosq;
	int rc = 0;
    int auth = 0;
    
wiringPiSetup();
pinMode(23, OUTPUT);
    

	signal(SIGINT, handle_signal);
	signal(SIGTERM, handle_signal);

	mosquitto_lib_init();

	mosq = mosquitto_new(clientid, true, 0);

	if(mosq){
		mosquitto_connect_callback_set(mosq, connect_callback);
		mosquitto_message_callback_set(mosq, message_callback);

        auth = mosquitto_username_pw_set(mosq,"admin", "zXVc5rCp7Nsho9EV");
        if(auth!=MOSQ_ERR_SUCCESS)
            cout<<"auth error"<<endl;

	    rc = mosquitto_connect(mosq, mqtt_host, mqtt_port, 60);

		mosquitto_subscribe(mosq, NULL, "testtopic/modem", 0);
        //  const char* state ="message1";
        //  mosquitto_publish(mosq,NULL,"testtopic/modem",sizeof(state),state, 2,false);
        // state ="message2";
        //  mosquitto_publish(mosq,NULL,"testtopic/modem",sizeof(state),state, 2,false);

		while(run){
			rc = mosquitto_loop(mosq, -1, 1);
			if(rc!= MOSQ_ERR_SUCCESS){
				printf("connection error!\n");
				sleep(10);
				mosquitto_reconnect(mosq);
			}
		}
		mosquitto_destroy(mosq);
	}

	mosquitto_lib_cleanup();

	return rc;
}